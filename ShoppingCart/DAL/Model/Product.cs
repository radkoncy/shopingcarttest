﻿namespace DAL.Model
{
	public class Product
	{
		public int Id { get; set; }
		public string ProductCode { get; set; }
		public string Description { get; set; }
		public decimal UnitPrice { get; set; }
	}
}
