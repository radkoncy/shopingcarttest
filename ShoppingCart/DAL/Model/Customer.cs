﻿namespace DAL.Model
{
	public class Customer
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public CustomerType Type { get; set; }
		public string Address { get; set; }
		public string Email { get; set; }

		public string Login { get; set; }
		public string Password { get; set; }
	}
}
