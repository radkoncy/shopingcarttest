﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model
{
	public class Order
	{
		public int Id { get; set; }
		public int CustomerId { get; set; }
		public Customer Customer { get; set; }
		public DateTime DateOpened { get; set; }
		public bool Finalized { get; set; }
		public DateTime DateFinalized { get; set; }

		public List<OrderItem> OrderItems { get; set; }
	}
}
