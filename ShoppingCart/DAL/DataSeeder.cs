﻿using DAL.Model;
using System;
using System.Collections.Generic;

namespace DAL
{
	class DataSeeder
	{
		static internal void Seed(IDataContext context)
		{
			AddOrders(context);
			AddProducts(context);
			AddCustomers(context);
		}

		private static void AddCustomers(IDataContext context)
		{
			if(context.Customers == null)
				context.Customers = new List<Customer>();

			context.Customers.Add(new Customer()
			{
				Login = "jon",
				Password = "jon",
				Email = "jon@gmail.com",
				Name = "Jon Johnson",
				Type = CustomerType.Gold,
				Address = "GREAT YELDHAM 19 Walden Road CO9 1QP"
			});

			context.Customers.Add(new Customer()
			{
				Login = "amanda",
				Password = "amanda",
				Email = "amanda@gmail.com",
				Name = "Amanda Smith",
				Type = CustomerType.Silver,
				Address = "London 19 Mill Road NW1 1AA"
			});
		}

		private static void AddOrders(IDataContext context)
		{
			if (context.Orders == null)
				context.Orders = new List<Order>();

			if (context.OrderItems == null)
				context.OrderItems = new List<OrderItem>();


			context.Orders.Add(new Order()
			{
				CustomerId = 1,
				Finalized = true,
				DateOpened = new DateTime(2017, 06, 01, 10, 10, 00),
				DateFinalized = new DateTime(2017, 06, 01, 12, 11, 00),
			});

			context.Orders.Add(new Order()
			{
				CustomerId = 2,
				Finalized = true,
				DateOpened = new DateTime(2017, 05, 11, 10, 10, 00),
				DateFinalized = new DateTime(2017, 05, 11, 12, 11, 00),
			});


			context.OrderItems.Add(new OrderItem()
			{
				OrderId = 1,
				ProductId = 1,
				Quantity = 40
			});

			context.OrderItems.Add(new OrderItem()
			{
				OrderId = 1,
				ProductId = 2,
				Quantity = 60
			});

			context.OrderItems.Add(new OrderItem()
			{
				OrderId = 2,
				ProductId = 1,
				Quantity = 60
			});
		}

		private static void AddProducts(IDataContext context)
		{
			if (context.Products == null)
				context.Products = new List<Product>();

			context.Products.Add(new Product()
			{
				ProductCode = "wine-x12",
				Description = "Wine 123",
				UnitPrice = 10
			});

			context.Products.Add(new Product()
			{
				ProductCode = "wine-xab",
				Description = "Wine abc",
				UnitPrice = 10
			});

			context.Products.Add(new Product()
			{
				ProductCode = "wine-x20",
				Description = "Wine 2016 Red Glass",
				UnitPrice = 15
			});

			context.Products.Add(new Product()
			{
				ProductCode = "wine-xdo",
				Description = "Wine Dodo",
				UnitPrice = 20
			});
		}
	}
}
