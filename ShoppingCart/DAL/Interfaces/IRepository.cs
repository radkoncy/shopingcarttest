﻿using System.Collections.Generic;
using System.Linq;

namespace DAL.Interfaces
{
	public interface IRepozitory<TEntity> where TEntity : class
	{
		List<TEntity> FetchAll();
		IQueryable<TEntity> Query { get; }
		void Add(TEntity entity);
		void Delete(TEntity entity);
		void Save();
	}
}
