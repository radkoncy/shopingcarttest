﻿using DAL.Model;
using System.Collections.Generic;

namespace DAL
{
	public class DataContext : IDataContext
	{
		public List<Order> Orders { get; set; }
		public List<Product> Products { get; set; }
		public List<Customer> Customers { get; set; }
		public List<OrderItem> OrderItems { get; set; }

		public DataContext()
		{
			DataSeeder.Seed(this);
		}
	}
}
