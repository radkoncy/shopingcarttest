﻿using DAL.Model;
using System.Collections.Generic;

namespace DAL
{
	public interface IDataContext
	{
		List<Order> Orders { get; set; }
		List<Product> Products { get; set; }
		List<Customer> Customers { get; set; }
		List<OrderItem> OrderItems { get; set; }
	}
}
