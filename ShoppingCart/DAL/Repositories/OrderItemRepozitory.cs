﻿using DAL.Interfaces;
using DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Repositories
{
	public class OrderItemRepozitory : IRepozitory<OrderItem>
	{
		private IDataContext db;

		public OrderItemRepozitory()
		{
			//After we install Ninject, it will be passed as parameters of this contructor
			db = new DataContext();
		}

		public IQueryable<OrderItem> Query
		{
			get
			{
				return db?.OrderItems?.AsQueryable();
			}
		}

		public void Add(OrderItem entity)
		{
			db.OrderItems.Add(entity);
		}

		public void Delete(OrderItem entity)
		{
			throw new NotImplementedException();
		}

		public List<OrderItem> FetchAll()
		{
			return db?.OrderItems;
		}

		public void Save()
		{
			throw new NotImplementedException();
		}
	}
}
