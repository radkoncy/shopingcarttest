﻿using DAL.Interfaces;
using DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
	public class ProductRepozitory : IRepozitory<Product>
	{
		private IDataContext db;

		public ProductRepozitory()
		{
			//After we install Ninject, it will be passed as parameters of this contructor
			db = new DataContext();
		}

		public IQueryable<Product> Query
		{
			get
			{
				return db.Products.AsQueryable();
			}
		}

		public void Add(Product entity)
		{
			db.Products.Add(entity);
		}

		public void Delete(Product entity)
		{
			throw new NotImplementedException();
		}

		public List<Product> FetchAll()
		{
			return db?.Products;
		}

		public void Save()
		{
			throw new NotImplementedException();
		}
	}
}
