﻿using DAL.Interfaces;
using DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Repositories
{
	public class OrderRepozitory : IRepozitory<Order>
	{
		private IDataContext db;

		public OrderRepozitory()
		{
			//After we install Ninject, it will be passed as parameters of this contructor
			db = new DataContext();
		}

		public IQueryable<Order> Query
		{
			get
			{
				return db?.Orders?.AsQueryable();
			}
		}

		public void Add(Order entity)
		{
			throw new NotImplementedException();
		}

		public void Delete(Order entity)
		{
			throw new NotImplementedException();
		}

		public List<Order> FetchAll()
		{
			return db?.Orders;
		}

		public void Save()
		{
			throw new NotImplementedException();
		}
	}
}
