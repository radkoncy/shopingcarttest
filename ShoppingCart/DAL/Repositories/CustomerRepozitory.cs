﻿using DAL.Interfaces;
using DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Repositories
{
	public class CustomerRepozitory : IRepozitory<Customer>
	{
		private IDataContext db;

		public CustomerRepozitory()
		{
			//After we install Ninject, it will be passed as parameters of this contructor
			db = new DataContext();
		}

		public IQueryable<Customer> Query
		{
			get
			{
				return db?.Customers?.AsQueryable();
			}
		}

		public void Add(Customer entity)
		{
			db.Customers.Add(entity);
		}

		public void Delete(Customer entity)
		{
			throw new NotImplementedException();
		}

		public List<Customer> FetchAll()
		{
			return db?.Customers;
		}

		public void Save()
		{
			throw new NotImplementedException();
		}
	}
}
