﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShoppingCart.Helpers;

namespace ShoppingCartTests.Helpers
{
	[TestClass]
	public class AuthorisationHelperTests
	{
		AuthorisationHelper authHelper;

		[TestInitialize]
		public void Init()
		{
			authHelper = new AuthorisationHelper();
		}

		[TestMethod]
		public void WrongAuthTest()
		{
			var token = authHelper.AuthoriseUser("jon", "jon");

			Assert.AreEqual(token, -1);
		}

		[TestMethod]
		public void CorrectAuthTest()
		{
			var token = authHelper.AuthoriseUser("test", "test");

			Assert.AreEqual(token, 12345);
		}
	}
}
