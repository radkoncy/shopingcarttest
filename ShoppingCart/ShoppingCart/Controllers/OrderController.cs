﻿using DAL.Interfaces;
using DAL.Model;
using DAL.Repositories;
using ShoppingCart.Helpers;
using ShoppingCart.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace ShoppingCart.Controllers
{
    public class OrderController : ApiController
	{
		IRepozitory<Order> orderRepozitory;
		IRepozitory<Product> productRepozitory;
		IRepozitory<Customer> customerRepozitory;
		IRepozitory<OrderItem> orderItemRepozitory;
		IAuthorisationHelper authHelper;

		public OrderController()
		{
			//After we install Ninject, it will be passed as parameters of this contructor
			authHelper = new AuthorisationHelper();
			orderRepozitory = new OrderRepozitory();
			productRepozitory = new ProductRepozitory();
			customerRepozitory = new CustomerRepozitory();
			orderItemRepozitory = new OrderItemRepozitory();
		}

		public List<Order> GetOrders()
		{
			return orderRepozitory.FetchAll();
		}

		[ResponseType(typeof(void))]
		public IHttpActionResult AddToBasket(int token, string productCode, int quantity)
		{
			if (!authHelper.IsTokenValid(token))
				return StatusCode(HttpStatusCode.Unauthorized);

			var order = GetOrderFromRepo(token);
			var product = productRepozitory.Query.FirstOrDefault(p => p.ProductCode == productCode);

			var orderItem = new OrderItem()
			{
				Id = productRepozitory.Query.Max(x => x.Id) + 1,
				Order = order,
				OrderId = order.Id,
				Product = product,
				ProductId = product.Id,
				Quantity = quantity
			};

			orderItem.Id = orderItemRepozitory.Query.Max(x => x.Id) + 1;
			orderItem.OrderId = order.Id;

			orderItemRepozitory.Add(orderItem);
			//TODO: After adding item to basket - check add change if needed the user type to Silver or Gold

			return StatusCode(HttpStatusCode.NoContent);
		}

		public IEnumerable<OrderLine> GetOrderLines(int token)
		{
			if (!authHelper.IsTokenValid(token))
				throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.Unauthorized));

			var order = GetOrderFromRepo(token);

			var orderLines = from item in order.OrderItems
							 select new OrderLine()
							 {
								 OrderId = order.Id,
								 ProductCode = item.Product.ProductCode,
								 Quantity = item.Quantity,
								 UnitPrice = item.Product.UnitPrice
							 };
			
			return orderLines;
		}

		private Order GetOrderFromRepo(int token)
		{
			var order = orderRepozitory.Query.FirstOrDefault(x => x.Finalized == false);

			if (order == null)
			{
				var customerLogin = authHelper.GetUserLoginByToken(token);
				var customer = customerRepozitory.Query.FirstOrDefault(x => x.Login == customerLogin);

				order = new Order()
				{
					CustomerId = customer.Id,
					DateOpened = DateTime.Now,
					Finalized = false,
					Id = orderItemRepozitory.Query.Max(x => x.Id) + 1
				};

				orderRepozitory.Add(order);
			}

			return order;
		}
	}
}
