﻿using ShoppingCart.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ShoppingCart.Controllers
{
	public class AuthController : ApiController
	{
		IAuthorisationHelper authHelper;

		public AuthController()
		{
			authHelper = new AuthorisationHelper();
		}
		
		[HttpGet]
		public int Authorise(string login, string password)
		{
			var token = authHelper.AuthoriseUser(login, password);
			return token;
		}
	}
}
