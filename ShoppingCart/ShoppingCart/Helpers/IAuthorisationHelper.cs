﻿namespace ShoppingCart.Helpers
{
	public interface IAuthorisationHelper
	{
		int AuthoriseUser(string login, string pass);
		bool IsTokenValid(int token);
		string GetUserLoginByToken(int token);
	}
}