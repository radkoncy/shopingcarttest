﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShoppingCart.Helpers
{
	public class AuthorisationHelper : IAuthorisationHelper
	{
		List<Tuple<int, string>> AuthorisedTokens = new List<Tuple<int, string>>();

		public int AuthoriseUser(string login, string pass)
		{
			if (login != "test" || pass != "test")
				return -1;

			var token = 12345;
			AuthorisedTokens.Add(new Tuple<int, string>(token, login));

			return token;
		}

		public bool IsTokenValid(int token)
		{
			return AuthorisedTokens.Any(x=>x.Item1 == token);
		}

		public string GetUserLoginByToken(int token)
		{
			return AuthorisedTokens.FirstOrDefault(x => x.Item1 == token)?.Item2;
		}
	}
}