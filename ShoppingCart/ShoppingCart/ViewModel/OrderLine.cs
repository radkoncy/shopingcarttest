﻿namespace ShoppingCart.ViewModel
{
	public class OrderLine
	{
		public int OrderId { get; set; }
		public string ProductCode { get; set; }
		public int Quantity { get; set; }
		public decimal UnitPrice { get; set; }
	}
}